<?php
require_once 'Fmi.php';
$fmi = new zachu\FMI(101009);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example usage of PHP-FMI</title>
</head>
<body>
    <dl>
        <dt>Time and Place</dt><dd><?=$fmi->getTimestamp();?> <?=$fmi->getLocation();?></dd>
        <dt>Temperature</dt><dd><?=$fmi->getLatestValue('temperature');?> °C</dd>
        <dt>Rain</dt><dd><?=$fmi->getLatestValue('rain');?> mm / <?=$fmi->getLatestValue('rain intensity');?></dd>
        <dt>Snow</dt><dd><?=$fmi->getLatestValue('snow depth');?> cm</dd>
        <dt>Wind</dt><dd>
            <?=$fmi->getLatestValue('wind speed');?> m/s /
            <?=$fmi->compassDirection($fmi->getLatestValue('wind direction'));?>
            (<?=$fmi->getLatestValue('gust speed');?> m/s gusts)
        </dd>
        <dt>Air pressure</dt><dd><?=$fmi->getLatestValue('air pressure');?> hPa</dd>
        <dt>Humidity</dt><dd><?=$fmi->getLatestValue('humidity');?> %</dd>
    </dl>
</body>
</html>
