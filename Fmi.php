<?php namespace zachu;

class FMI
{
    const CACHE_FILE = '.fmi.cache';
    const CACHE_EXPIRE = 5 * 60;

    // 100968 Vantaa Helsinki-Vantaan lentoasema
    // 101009 Helsinki Malmi lentokenttä
    const URL = <<<'EOT'
        https://opendata.fmi.fi/wfs?
        request=getFeature&
        storedquery_id=fmi%3A%3Aobservations%3A%3Aweather%3A%3Atimevaluepair&
        fmisid={{location}}
        EOT;

    const COMPASS_DIRS = [
        '⬇',
        '↙',
        '⬅',
        '↖',
        '⬆',
        '↗',
        '➡',
        '↘',
    ];

    const KEYS = [
        't2m' => 'temperature', // °C
        'wg_10min' => 'gust speed', // m/s
        'wd_10min' => 'wind direction', // deg
        'ws_10min' => 'wind speed', // m/s
        'rh' => 'humidity', // %
        'td' => 'dew point', // °C
        'r_1h' => 'rain', // mm
        'ri_10min' => 'rain intensity', //
        'snow_aws' => 'snow depth', // cm maybe?
        'p_sea' => 'air pressure', // hPa
        'vis' => 'visibility', // m
        // 'n_man'    => 'No idea',
        // 'wawa'     => 'No idea2',
    ];
    const ID_PREFIX = 'obs-obs-1-1-';

    protected $parsed = [];
    protected $location;
    public $debug;
    public function __construct(string $location, $debug = false)
    {
        $this->location = $location;
        $this->debug = $debug;
        $data = $this->loadData();
        foreach (self::KEYS as $key => $value) {
            if ($this->debug) {
                $this->debugmsg("Parsing results for ${value} ($key)");
            }

            $this->parsed[$value] = $this->getMeasurements($data, self::ID_PREFIX . $key);
        }

        $this->parsed['location'] = $this->parseLocation($data);
        $this->parsed['timestamp'] = $this->getLatest(array_values(self::KEYS)[0])['time'];
    }

    private function debugmsg(string $message)
    {
        echo "<pre>${message}</pre>";
    }

    protected function loadData()
    {
        if (!($data = $this->loadCache())) {
            if ($this->debug) {
                $expires = time() - filemtime(self::CACHE_FILE);
                $this->debugmsg("Cache miss. Loading data from API");
            }

            $url = str_replace(["\n", "{{location}}"], ["", $this->location], self::URL);
            $data = file_get_contents($url);
            $this->saveCache($data);
        }

        return simplexml_load_string($data);
    }

    protected function loadCache()
    {
        $data = false;
        if (file_exists(self::CACHE_FILE) and (time() - filemtime(self::CACHE_FILE)) < self::CACHE_EXPIRE) {
            if ($this->debug) {
                $expires = self::CACHE_EXPIRE - (time() - filemtime(self::CACHE_FILE));
                $this->debugmsg("Cache hit. Cache still alive for ${expires} seconds");
            }
            $data = file_get_contents(self::CACHE_FILE);
        }

        return $data;
    }

    protected function saveCache(string $data)
    {
        file_put_contents(self::CACHE_FILE, $data);
    }

    protected function getMeasurements(\SimpleXMLElement $data, string $id)
    {
        $rows = $data->xpath(<<<EOT
            /wfs:FeatureCollection
            /wfs:member
            /omso:PointTimeSeriesObservation
            /om:result
            /wml2:MeasurementTimeseries[@gml:id="${id}"]
            /wml2:point
            /wml2:MeasurementTVP
        EOT);

        $values = [];
        foreach ($rows as $row) {
            $values[] = [
                'time' => (string) $row->xpath('wml2:time')[0],
                'value' => (float) $row->xpath('wml2:value')[0],
            ];
        }
        return $values;
    }

    protected function parseLocation(\SimpleXMLElement $data)
    {
        return (string) $data->xpath(<<<EOT
            /wfs:FeatureCollection
            /wfs:member[1]
            /omso:PointTimeSeriesObservation
            /om:featureOfInterest
            /sams:SF_SpatialSamplingFeature
            /sam:sampledFeature
            /target:LocationCollection
            /target:member
            /target:Location
            /gml:name
        EOT)[0];
    }

    public function compassDirection(int $degrees)
    {
        $degrees = $degrees % 360;
        $index = round($degrees / (360 / count(self::COMPASS_DIRS)), 0);
        return self::COMPASS_DIRS[$index];
    }

    public function getLatest(string $measurement)
    {
        return end($this->parsed[$measurement]);
    }

    public function getLatestValue(string $measurement)
    {
        return $this->getLatest($measurement)['value'];
    }

    public function getLocation()
    {
        return $this->parsed['location'];
    }
    public function getTimestamp()
    {
        return $this->parsed['timestamp'];
    }
}
